import { BaseModel, beforeSave, column } from '@ioc:Adonis/Lucid/Orm'
import KSUID from 'ksuid'
import { DateTime } from 'luxon'

const generateRandomTokenId = async () => {
  return (await KSUID.random()).string
}

export default class Token extends BaseModel {
  public static table = 'Tokens'

  @column({ isPrimary: true })
  public id: number

  @column({})
  public token: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoUpdate: true })
  public updatedAt: DateTime

  @column({ columnName: 'origin_name', serializeAs: 'originName' })
  public originName: string

  @column({ columnName: 'origin_id', serializeAs: 'originId' })
  public originId: string

  @column({})
  public uid: string

  @beforeSave()
  public static async generateRandomTokenId(token: Token) {
    if (token.$isNew) {
      token.token = await generateRandomTokenId()
    }
  }
}
