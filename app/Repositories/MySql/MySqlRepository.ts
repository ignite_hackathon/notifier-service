import Token from 'App/Models/Token'

const MySqlRepository = {
  find: async (value) => {
    return Token.find(value)
  },
  findBy: async (key, value) => {
    return Token.findBy(key, value)
  },
  findByAndUpdate: async (key, value, payload) => {
    const token = await Token.findBy(key, value)
    if (!token) return null

    await token.merge(payload).save()

    return token
  },
  findByIdAndUpdate: async (id, payload) => {
    const token = await Token.findOrFail(id)

    await token.merge(payload).save()

    return token
  },
  create: async (values) => {
    return Token.create(values)
  },
}

export default MySqlRepository
