import Logger from '@ioc:Adonis/Core/Logger'
import MySqlRepository from 'App/Repositories/MySql/MySqlRepository'

export default class TokensController {
  public async getToken({ params }) {
    const { uid, originName = 'Telegram' } = params
    let tokenRegister = await MySqlRepository.findBy('uid', uid)

    if (tokenRegister) {
      Logger.info('User already generated token')
      return tokenRegister
    }

    Logger.info('Save user notification token')
    return MySqlRepository.create({
      uid: params.uid,
      originName,
    })
  }
}
