import TelegramBot from 'node-telegram-bot-api'
import Logger from '@ioc:Adonis/Core/Logger'
import MySqlRepository from 'App/Repositories/MySql/MySqlRepository'

const token = process.env.TELEGRAM_BOT_TOKEN

const bot = new TelegramBot(token, { polling: true })

bot.onText(/\/start (.+)/, async (msg, match) => {
  // 'msg' is the received Message from Telegram
  // 'match' is the result of executing the regexp above on the text content
  // of the message
  Logger.info(msg)
  const chatId = msg.chat.id
  const tokenId = match[1] // the captured "tokenId"

  const token = await MySqlRepository.findBy('token', tokenId)

  if (!token) {
    Logger.info('User notification token not exists')
    bot.sendMessage(chatId, 'Para comenzar a operar, entra en https://dashboard.nubemotic.com')
    return
  }

  if (token.originId) {
    Logger.info('User already subscribe')
    bot.sendMessage(
      chatId,
      'Ya estas subscripto al canal de notificaciones usa NADA para pedir ayuda'
    )
    return
  }

  const updatedToken = await MySqlRepository.findByIdAndUpdate(token.id, { originId: chatId })

  Logger.info(`User notification registered with tokenId: ${updatedToken.token}`)
  bot.sendMessage(chatId, 'Bienvenido!')
})

//  There are different kinds of
// messages.Listen for any kind of message.
bot.on('message', (msg) => {
  const chatId = msg.chat.id

  Logger.info(`Message Received ${msg} from ${chatId}`)

  // send a message to the chat acknowledging receipt of their message
  // bot.sendMessage(chatId, 'No te entendo')
})
